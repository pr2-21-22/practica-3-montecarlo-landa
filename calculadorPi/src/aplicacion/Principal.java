package aplicacion;

import java.util.stream.*;
import mates.Matematicas;

/** <h1> Clase Principal <h1>
 *  La clase principal llama al metodo getPi, y le entrega el valor de entrada que recibe por consola. 
 */
public class Principal{
        public static void main(String[] args){
  	      System.out.println("El numero PI es " + Matematicas.getPi(Integer.parseInt(args[0])));
        }
}
