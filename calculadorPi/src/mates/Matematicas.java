package mates;
import java.util.stream.*;

public class Matematicas{
        public static double getPi(long pasos){
                return ((double)(Stream.generate(() -> Math.sqrt(Math.pow(Math.random(), 2)+ Math.pow(Math.random(), 2)))
                .limit(pasos)
                .filter(x-> x<1)
                .count())/pasos)*4;
        }
}      
